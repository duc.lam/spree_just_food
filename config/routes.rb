Spree::Core::Engine.routes.draw do
  namespace :admin, path: Spree.admin_path do
    resource :just_food_settings, only: [:edit, :update] do
      post :testorder, on: :collection
    end
  end
end
