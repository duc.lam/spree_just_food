module Spree
  AppConfiguration.class_eval do
    preference :justfood_enable, :boolean, default: false
    preference :justfood_site_name, :string, default: nil
    preference :justfood_api_url, :string, default: nil
    preference :justfood_username, :string, default: nil
    preference :justfood_password, :string, default: nil
    preference :justfood_customer_no, :string, default: nil
    preference :justfood_gl_account, :string, default: nil
    preference :justfood_email, :string, default: 'it-group@naturesflavors.com'
    preference :justfood_test_order_no, :string, default: nil
  end
end
