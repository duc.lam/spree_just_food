Spree::Order.class_eval do

  def just_food_create_sales_order(bypass_enable=false)
    begin
      justfood_enable = Spree::Config.justfood_enable
      justfood_site_name = Spree::Config.justfood_site_name
      justfood_api_url = Spree::Config.justfood_api_url
      justfood_username = Spree::Config.justfood_username
      justfood_password = Spree::Config.justfood_password
      justfood_customer_no = Spree::Config.justfood_customer_no
      justfood_gl_account = Spree::Config.justfood_gl_account
      justfood_email = Spree::Config.justfood_email

      if justfood_email.empty?
        justfood_email = 'it-group@naturesflavors.com'
      end

      if !justfood_enable && !bypass_enable
        subject = "JustFood/#{justfood_site_name}: code is NOT enabled. DID NOT post Order ##{self.number}."
        Spree::JustFoodMailer.send_message(justfood_email, subject, "").deliver_later
        Rails.logger.info subject
        return
      end

      if justfood_site_name.empty? || justfood_api_url.empty? || justfood_username.empty? || justfood_password.empty? || justfood_customer_no.empty?
        subject = "JustFood/#{justfood_site_name}: configuration not set. DID NOT post Order ##{self.number}."
        Spree::JustFoodMailer.send_message(justfood_email, subject, "").deliver_later
        Rails.logger.info subject
        return
      end

      # soap client
      client = JustFoodAPI::V1::Client.new(justfood_site_name, justfood_api_url, justfood_username, justfood_password, justfood_customer_no, justfood_gl_account)

      # check if sales order already exists
      begin
        response = client.read_multiple(self.number)
        readmultiple_result = response.body[:read_multiple_result][:read_multiple_result]

        if !readmultiple_result.nil? && readmultiple_result.size > 0
          justfood_sales_order_no = readmultiple_result[:np_sales_order][:no]
          subject = "JustFood/#{justfood_site_name}: Order ##{self.number} already exists as JF Sales Order ##{justfood_sales_order_no}. Will NOT post again."
          Spree::JustFoodMailer.send_message(justfood_email, subject, "").deliver_later
          Rails.logger.info subject
          return false;
        end  
      rescue StandardError => e
        subject = "JustFood/#{justfood_site_name}: Order ##{self.number} FAILED to check if sales order exists. Will post order again."
        Spree::JustFoodMailer.send_message(justfood_email, subject, e.message).deliver_later
        Rails.logger.info "#{subject} StandardError: #{e.message}"
      end

      # create sales order
      response = client.create(self)
      justfood_sales_order_no = response.body[:create_result][:np_sales_order][:no];
      subject = "JustFood/#{justfood_site_name}: Order ##{self.number} posted SUCCESSFULLY. JF Sales Order ##{justfood_sales_order_no}"
      Spree::JustFoodMailer.send_message(justfood_email, subject, "").deliver_later
      Rails.logger.info subject

      # update database with sales order no
      begin
        if self.update_attributes(justfood_sales_order_no: justfood_sales_order_no)
          subject = "JustFood/#{justfood_site_name}: Order ##{self.number} SUCCESSFULLY updated justfood_sales_order_no field. JF Sales Order ##{justfood_sales_order_no}"
          Spree::JustFoodMailer.send_message(justfood_email, subject, "").deliver_later
          Rails.logger.info subject
        else
          subject = "JustFood/#{justfood_site_name}: Order ##{self.number} FAILED to update database justfood_sales_order_no field. JF Sales Order ##{justfood_sales_order_no}"
          Spree::JustFoodMailer.send_message(justfood_email, subject, "").deliver_later
          Rails.logger.info subject
        end
      rescue StandardError => e
        subject = "JustFood/#{justfood_site_name}: Order ##{self.number} FAILED to update database justfood_sales_order_no field. JF Sales Order ##{justfood_sales_order_no}."
        Spree::JustFoodMailer.send_message(justfood_email, subject, e.message).deliver_later
        Rails.logger.info "#{subject} StandardError: #{e.message}"
      end
    rescue StandardError => e
      subject = "JustFood/#{justfood_site_name}: Order ##{self.number} FAILED to post."
      Spree::JustFoodMailer.send_message(justfood_email, subject, e.message).deliver_later
      Rails.logger.info "#{subject} StandardError: #{e.message}"
    end
  end

end

#Spree::Order.state_machine.after_transition :to => :complete,
#                                            :do => :just_food_create_sales_order