require 'savon'

module JustFoodAPI
  module V1
    class Client
      
      def initialize(justfood_site_name, justfood_api_url, justfood_username, justfood_password, justfood_customer_no, justfood_gl_account)
        @justfood_site_name = justfood_site_name
        @justfood_api_url = URI::escape(justfood_api_url)
        @justfood_username = justfood_username
        @justfood_password = justfood_password
        @justfood_customer_no = justfood_customer_no
        @justfood_gl_account = justfood_gl_account

        @client = Savon.client(
          wsdl: @justfood_api_url,
          endpoint: @justfood_api_url,
          basic_auth: [@justfood_username, @justfood_password]
        )
      end

      def create(order)
        sales_lines = [];
        order.line_items.order('id ASC').each do |line_item|
          sku = line_item.variant.sku.upcase
          quantity = line_item.quantity
          unit_price = line_item.price

          sales_lines << {
            'Type' => 'Item',
            'No' => sku,
            'Quantity' => quantity,
            'Unit_of_Measure_Code' => 'EA',
            'Unit_Price' => unit_price
          }
        end

        # Add shipping as a line item
        if order.ship_total > 0.0
          sales_lines << {
            'Type' => 'G_L_Account',
            'No' => @justfood_gl_account,
            'Quantity' => 1,
            'Unit_of_Measure_Code' => 'EA',
            'Unit_Price' => order.ship_total
          }
        end
        
        # Add adjustment/promos as a line item
        if order.adjustment_total != 0.0
          sales_lines << {
            'Type' => 'G_L_Account',
            'No' => 4100,
            'Description' => 'Promotions',
            'Quantity' => 1,
            'Unit_of_Measure_Code' => 'EA',
            'Unit_Price' => order.adjustment_total
          }
        end
        
        order_date = order.completed_at.strftime("%Y-%m-%d")
        today_date = Time.now.strftime("%Y-%m-%d")
        document_date = today_date
        
        if Date.parse(order_date) < Date.parse(today_date)
          post_date = today_date;
          ship_date = add_working_days(post_date);
        else # i.e. order_date == today_date
          post_date = order_date;
          ship_date = add_working_days(post_date);
        end
        due_date = ship_date;

        params = {
          'NPSalesOrder' => {
            'Sell_to_Customer_No' => @justfood_customer_no,
            'Document_Date' => document_date,
            'Posting_Date' => post_date,
            'Order_Date' => order_date,
            'Due_Date' => due_date,
            'External_Document_No' => order.number,
            'Shipment_Date' => ship_date,
            'SalesLines' => {
              'IBNEW_SalesOrderSubformWS' => sales_lines
            }
          }
        }
        
        response = @client.call(:create, message: params)
        
        return response
      end
      
      def read_multiple(external_document_no)
        params = {
          'filter' => {
            'Field' => 'External_Document_No',
            'Criteria' => external_document_no,
          },
          'setSize' => 1
        }
    
        response = @client.call(:read_multiple, message: params);

        return response;
      end

      private

      def add_working_days(curdate)
        date_obj = Date.parse(curdate)

        case date_obj.wday
        when 0, 1, 2, 3 # sun-wed
          days = 2
        when 4 # thur
          days = 5
        when 5 # fri
          days = 4
        when 6 # sat
          days = 3
        end

        return date_obj.next_day(days).strftime("%Y-%m-%d")
      end
  
    end
  end
end
