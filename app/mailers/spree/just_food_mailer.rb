module Spree
  class JustFoodMailer < BaseMailer

    def send_message(to_address, subject, message)
      @message = message

      mail(to: to_address, from: from_address, subject: subject)
    end
    
  end
end
