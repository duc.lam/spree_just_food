module Spree
  module Admin
    class JustFoodSettingsController < Spree::Admin::BaseController
      
      def update
        params.each do |name, value|
          next unless Spree::Config.has_preference? name
          Spree::Config[name] = value
        end

        flash[:success] = Spree.t(:successfully_updated, resource: Spree.t(:just_food_settings))
        redirect_to edit_admin_just_food_settings_url
      end

      def testorder()
        justfood_test_order_no = Spree::Config.justfood_test_order_no
        
        if justfood_test_order_no.empty?
          flash[:error] = Spree.t('just_food.testorders.set_test_orders')
          return redirect_to edit_admin_just_food_settings_url
        end

        success = true
        failed_order_nums = []
        justfood_test_order_no.split(',').each do |order_no|
          order = Spree::Order.find_by(number: order_no)
          if order
            order.just_food_create_sales_order(true)
          else
            failed_order_nums << order_no
            success = false
          end
        end

        if success
          flash[:success] = Spree.t('just_food.testorders.check_email_for_post_messages')
          return redirect_to edit_admin_just_food_settings_url
        else
          flash[:error] = Spree.t('just_food.testorders.can_not_find_orders', nums: failed_order_nums.join(', '))
          return redirect_to edit_admin_just_food_settings_url
        end
      end
    end
  end
end
