class AddJustFoodSalesOrderNoToSpreeOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_orders, :justfood_sales_order_no, :string
  end
end
